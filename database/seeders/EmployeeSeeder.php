<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Employee;
use DB;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Employee::truncate();
    	
    // for multiple record entries

    	DB::table('employees')->insert([
	    [
	        	'name' => 'ankita',
		        'email' => 'ankita@gmail.com',
		        'phone' => '1234567898',
		        'department' => 'hr',
		        'dob' => '2010-01-08',
	    ],
	    [
	        	'name' => 'anita',
		        'email' => 'anita@gmail.com',
		        'phone' => '1234567790',
		        'department' => 'junior',
		        'dob' => '2010-01-04',
	    ],
	    [
	        	'name' => 'kartik',
		        'email' => 'kartik@gmail.com',
		        'phone' => '1234367890',
		        'department' => 'junior-hr',
		        'dob' => '2010-01-04',
	    ],
	    [
	        	'name' => 'James',
		        'email' => 'James@gmail.com',
		        'phone' => '1234563890',
		        'department' => 'senior',
		        'dob' => '2010-01-02',
	    ],
	    [
	        	'name' => 'amita',
		        'email' => 'amita@gmail.com',
		        'phone' => '1234767890',
		        'department' => 'hr',
		        'dob' => '2010-01-09',
	    ],

]);

    //for single record entry

      //   Employee::create([
      //   'name' => 'John Doe',
      //   'email' => 'doejohn@gmail.com',
      //   'phone' => '1234567890',
      //   'department' => 'hr',
      //   'dob' => '2010-01-03',
     	// ]);
    }
}
            