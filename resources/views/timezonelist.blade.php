<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Laravel Generate Timezone list in dropdownwith Coding Driver</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />


<style>
.invalid-feedback {
  display: block;
}
</style>
</head>
<body>
	<div class="container mt-4">
  <h2>Laravel Generate Timezone list in dropdown with - <a href="https://codingdriver.com/">codingdriver.com</a></h2>
 
  <div class="row form-group">
      <div class="col-md-2">
          Timezone
      </div>
      <div class="col-md-8">
          <select class="form-control mb-2" name="timezone_id">
              <option value="">Please select...</option>
              @foreach($timezones as $timezone)
              <option value="{{ $timezone->id }}">{{ $timezone->name }} ({{ $timezone->offset }})</option>
              @endforeach
          </select>
      </div>
  </div>
</div>
</body>
</html>