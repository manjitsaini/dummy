<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Cashier\Cashier;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function test(){
        $stripe = new \Stripe\StripeClient(
          'sk_test_51IJa1QEes1u1weCRoHN6L6PTAOGmbZDUMqH8CR1C5n7yxkPUZAuGbdmfeno0QB6iS8CwqinHPzKrJ8cW44UJ8VL800RFzNEXCi'
        );
        $stripe->customers->create([
            'email' => "abcd@yopmail.com",
            'name' => "manu",
          'description' => 'My First Test Customer (created for API docs)',
        ]);

        // $arr = array(
        //     'source'   => 'bjhgjhghj',
        //     'email'    => 'abc@yopmail.com',
        //     'plan'     => 'monthly_recurring_setupfee',
        //     'account_balance' => 00,
        //     'description' => "Charge with one time setup fee"
        //     );

    // $plan = Plans::where('identifier', $request->plan)
    //         ->orWhere('identifier', 'basic')
    //         ->first();
        
    //     $request->user()->newSubscription('default', $plan->stripe_id)->create($request->token);
        // dd((object)$arr->user()->newSubscription('default', "pk_test_51IJa1QEes1u1weCR2LInw4LHLf0ZpeOtB2cLdlJpSd3W5wGRDPCMrMZBiIu6FIrPFN8J7hn1ojogQJPuT0LCRoZd00pcFRTjIV")->create());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
