<?php

namespace App\Http\Controllers;

use App\Models\Timezone;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function create(){
    	$timezones = Timezone::Orderby('offset')->get();
    	return view('timezonelist', compact('timezones')); 
    }
}
